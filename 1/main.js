'use strict';

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
 * The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
 **/

const MAX_EXCLUSIVE = 1000;

class Main {

    run() {
        console.time('totaltime');

        let total = 0;
        for (let n = 1; n < MAX_EXCLUSIVE; n++) {
            if ( (n % 3 === 0) || (n % 5 === 0)) {
                total += n;
            }
        }

        console.timeEnd('totaltime');
        console.log('Total natural numbers (%3 or %5) up to max %d is %d', MAX_EXCLUSIVE, total);

    }

}

module.exports = Main;
